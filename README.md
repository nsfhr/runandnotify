runandnotify
============

Bash script to run cron jobs and notify various targets upon failure or success.

Forked and modified from https://github.com/WEBclusive/runandnotify

Usage
-----

    Usage: /usr/local/bin/runandnotify -c <command> -d <description> -l <label>
    
    This script will execute the given command and send notices to various targets upon
    failure or success.
    
    Notices are configured in (in order of preference):
     ~/.runandnotify.conf
     /opt/cfs/runandnotify/conf/runandnotify.conf
     /opt/cfs/runandnotify/conf/runandnotify.conf.dist
    
    Currently supported are:
    - email (via /usr/bin/mail)
    - hipchat (http://www.hipchat.com)
    - deadmanssnitch (https://deadmanssnitch.com)
    
    OPTIONS:
    -h Show this message
    -t Do a test run (does not run the command but sends both success and failure notices)
    -c <command> the cli command to be executed
    -d <description> the description of the action to be used in the notices
       (like email subjects, HipChat messages)
    -l <label> the label of the action.
       Used as [Label] in email, from name in HipChat etcetera.
    -s <snitch id> ID of a deadmanssnitch to notify (only on success)
    -q Quiet mode, does not trigger notifications on success, just failure 
       (except of course for deadmeanssnitch)
    
Examples
--------

Duplicity backup, also pings deadmanssnitch

    /usr/local/bin/runandnotify -c "/usr/local/bin/duplicity-webclusive.sh backup" -d "Backup Passive Storage" -l Pluto -s 4d4079dfc9  &> /dev/null

Note that the snitch ID is passed as a parameter and not via the config because each cronjob has its own snitch.

Github mirror

    /usr/local/bin/runandnotify -c "/usr/local/bin/gitsnap" -d "GitHub Mirror" -l Pluto &> /dev/null    
    

Targets
-------

### Email

Email will be sent with subject in the following format:

    [<label>] <description> [failed!!!|success]
    
The body will contain the command it tried to run and the output of the command.

Failure and success notices can be sent to different email addresses.

### HipChat

The <label> will be used as the from-value, and the description with "failed" or "success" appended as the message.
Also, success notices will be green and failures in red (unless overriden in the config).

The output of the command will not be sent, so make sure you have logs or email notices, not just HipChat.

Failure and success notices can be sent to different rooms.

### Deadmanssnitch

The snitch will be pinged only if the command has run succesfully.

No message is passed at the moment.
