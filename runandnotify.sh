#!/bin/bash
EXITVALUE=0
CONFIG_DIR="/opt/symbid/etc"

CONF_DEFAULT="${CONFIG_DIR}/runandnotify.conf.dist"
CONF_LOCAL="${CONFIG_DIR}/runandnotify.conf"
CONF_USER="$HOME.runandnotify.conf"
CONF_DEV="./runandnotify.conf.dev"

snitchnotice()
{
    if [ -n "$SNITCH_ID" ]; then
        curl "https://nosnch.in/$SNITCH_ID"
    fi
}

slacknotice()
{
    if [ -n "$SLACK_CHANNEL" ]; then
        TEXT=$(echo ${MSG} | sed 's/"/\"/g' | sed "s/'/\'/g" )
        S_OUTPUT=$(echo "${OUTPUT}"  | sed 's/"/\"/g' | sed "s/'/\'/g" | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g" )
        S_COMMAND=$(echo ${COMMAND} | sed 's/"/\"/g' | sed "s/'/\'/g" )
        # ugly way of escaping linefeeds
        S_OUTPUT=${S_OUTPUT//
/\\\n}

#\"pretext\":\"pretext\",\
        ATT="[{\
\"fallback\":\"${TEXT}\",\
\"color\":\"${SLACK_COLOR}\",\
\"mrkdwn_in\": [\"value\", \"title\", \"fields\"],\
\"fields\":[{\
   \"title\":\"Command\",\
   \"value\":\"\`\`\`${S_COMMAND}\`\`\`\",\
   \"short\":false\
},{\
   \"title\":\"Output\",\
   \"value\":\"\`\`\`${S_OUTPUT}\`\`\`\",\
   \"short\":false\
}]\
}]"

        JSON="{\"channel\": \"#${SLACK_CHANNEL}\",\
\"text\": \"${TEXT}\",\
\"username\": \"${LABEL}\", \
\"mrkdwn\": true,\
\"attachments\": ${ATT}"

        if [ -n "$SLACK_EMOJI" ]; then
            JSON="${JSON},\"icon_emoji\": \"${SLACK_EMOJI}\""
        fi
        JSON="${JSON}}"

        echo "$JSON"
        curl -X POST --data-urlencode "payload=${JSON}" "https://${SLACK_DOMAIN}.slack.com/services/hooks/incoming-webhook?token=${SLACK_TOKEN}"
    fi
}

slacksuccess()
{
    if [ -n "$SLACK_COLOR_SUCCESS" ]; then
        SLACK_COLOR="$SLACK_COLOR_SUCCESS"
    fi
    if [ -n "$SLACK_CHANNEL_SUCCESS" ]; then
        SLACK_CHANNEL="$SLACK_CHANNEL_SUCCESS"
    fi
    MSG="$DESCR successful"
    slacknotice
}

slackfail()
{
    if [ -n "$SLACK_COLOR_FAIL" ]; then
        SLACK_COLOR="$SLACK_COLOR_FAIL"
    fi
    if [ -n "$SLACK_CHANNEL_FAIL" ]; then
        SLACK_CHANNEL="$SLACK_CHANNEL_FAIL"
    fi
    MSG="$DESCR failed"
    slacknotice
}

# Adapted from https://github.com/hipchat/hipchat-cli
hipchatnotice()
{   
    if [ -n "$HIPCHAT_ROOM" ]; then
        INPUT="$MSG"
    
        # replace newlines with XHTML <br>
        INPUT=$(echo -n "${INPUT}" | sed "s/$/\<br\>/")

        # replace bare URLs with real hyperlinks
        INPUT=$(echo -n "${INPUT}" | perl -p -e "s/(?<!href=\")((?:https?|ftp|mailto)\:\/\/[^ \n]*)/\<a href=\"\1\"\>\1\<\/a>/g")

        # urlencode with perl
        INPUT=$(echo -n "${INPUT}" | perl -p -e 's/([^A-Za-z0-9])/sprintf("%%%02X", ord($1))/seg')

        # do the curl
        curl -sS \
            -d "auth_token=$HIPCHAT_TOKEN&room_id=$HIPCHAT_ROOM&from=$LABEL&color=$HIPCHAT_COLOR&message=$INPUT&notify=$HIPCHAT_NOTIFY" \
            https://api.hipchat.com/v1/rooms/message
    fi
}

hipchatsuccess()
{
    if [ -n "$HIPCHAT_COLOR_SUCCESS" ]; then
        HIPCHAT_COLOR="$HIPCHAT_COLOR_SUCCESS"
    fi
    if [ -n "$HIPCHAT_NOTIFY_SUCCESS" ]; then
        HIPCHAT_NOTIFY="$HIPCHAT_NOTIFY_SUCCESS"
    fi
    if [ -n "$HIPCHAT_ROOM_SUCCESS" ]; then
        HIPCHAT_ROOM="$HIPCHAT_ROOM_SUCCESS"
    fi
    MSG="$DESCR successful"
    hipchatnotice
}

hipchatfail()
{
    if [ -n "$HIPCHAT_COLOR_FAIL" ]; then
        HIPCHAT_COLOR="$HIPCHAT_COLOR_FAIL"
    fi
    if [ -n "$HIPCHAT_NOTIFY_FAIL" ]; then
        HIPCHAT_NOTIFY="$HIPCHAT_NOTIFY_FAIL"
    fi
    if [ -n "$HIPCHAT_ROOM_FAIL" ]; then
        HIPCHAT_ROOM="$HIPCHAT_ROOM_FAIL"
    fi
    MSG="$DESCR failed"
    hipchatnotice
}

mailnotice()
{
    if [ -n "$MAIL_TO" ]; then
        M_TEXT="Result of '$COMMAND':\n\n$OUTPUT"
        echo -e "$M_TEXT" | $MAIL_CMD -s"[$LABEL] $MSG" "$MAIL_TO" &>/dev/null
    fi
}

mailsuccess()
{
    if [ -n "$MAIL_TO_SUCCESS" ]; then
        MAIL_TO="$MAIL_TO_SUCCESS"
    fi
    MSG="$DESCR successful"
    mailnotice
}

mailfail()
{
    if [ -n "$MAIL_TO_FAIL" ]; then
        MAIL_TO="$MAIL_TO_FAIL"
    fi
    MSG="$DESCR failed!!!"
    mailnotice
}

mailconfig()
{
    if [ -n "$MAIL_TO_FAIL" ] || [ -n "$MAIL_TO_SUCCESS" ] || [ -n "$MAIL_TO" ]; then
        if [ ! -x $MAIL_CMD ]; then
            echo #
            echo "$MAIL_CMD not found"
            echo "You may try 'sudo apt-get install mailutils'"
            echo #
            exit 1
        fi
    fi
}


# Loads configuration files in order:
# 1 - Global defaults from /etc
# 2 - Server settings from /etc
# 3 - User settings from ~/
# Any configs that don't exist will be skipped
loadconfig()
{
    if [ -r $CONF_DEFAULT ]; then
        echo "Loading config from $CONF_DEFAULT..."
        . "$CONF_DEFAULT"
    fi
    if [ -r $CONF_LOCAL ]; then
        echo "Loading config from $CONF_LOCAL..."
        . "$CONF_LOCAL"
    fi
    if [ -r $CONF_USER ]; then
        echo "Loading config from $CONF_USER..."
        . "$CONF_USER"
    fi

    if [ -r $CONF_DEV ]; then
        echo "Loading config from $CONF_DEV..."
        . "$CONF_DEV"
    fi
    
    mailconfig
}


runcommand()
{
    echo "Running: '$COMMAND'"
    OUTPUT="$($COMMAND)"
    EXITVALUE="$?"
    
    if [ "$EXITVALUE" = "0" ]; then
        notifysuccess
    else
        notifyfail
    fi
}

testcommand()
{
    echo "NOT running: '$COMMAND'"
    echo "successfully failed to do anything..."
    OUTPUT="Result of not running '$COMMAND'"
    notifyfail
    notifysuccess
}

notifyfail()
{
    echo #
    echo -e "$OUTPUT"
    echo "FAILURE"
    mailfail
    hipchatfail
    slackfail
}

notifysuccess()
{
    echo #
    echo -e "$OUTPUT"
    if ! $QUIET ; then
        mailsuccess
        hipchatsuccess
        slacksuccess
    fi
    snitchnotice
}

usage()
{
    cat << EOF
Usage: $0 -c <command> -d <description> -l <label>

This script will execute the given command and send notices to various targets upon
failure or success.

Notices are configured in (in order of preference):
 $CONF_USER
 $CONF_LOCAL
 $CONF_DEFAULT

Currently supported are:
- email (via /usr/bin/mail)
- hipchat (http://www.hipchat.com)
- deadmanssnitch (https://deadmanssnitch.com)
 
OPTIONS:
-h Show this message
-t Do a test run (does not run the command but sends both success and failure notices)
-c <command> the cli command to be executed
-d <description> the description of the action to be used in the notices
   (like email subjects, HipChat messages)
-l <label> the label of the action. 
   Used as [Label] in email, from name in HipChat etcetera.
-s <snitch id> ID of a deadmanssnitch to notify (only on success)
-q Quiet mode, does not trigger notifications on success, just failure 
   (except of course for deadmeanssnitch)
EOF
    echo #
}

echo #
echo "Symbid Run And Notify"
loadconfig
echo #

OS=$(uname -s)

ACTION="run"
COMMAND=
DESCR="Undefined process"
LABEL="RunAndNotify"
SNITCH_ID=
QUIET=false

while getopts “htqc:d:l:s:” OPTION; do
case $OPTION in
    h) usage; exit 1;;
    t) ACTION="test";;
    q) QUIET=true;;
    c) COMMAND=$OPTARG;;
    d) DESCR=$OPTARG;;
    l) LABEL=$OPTARG;;
    s) SNITCH_ID=$OPTARG;;
    [?]) usage; exit;;
  esac
done

if [ -z "$COMMAND" ]; then
    usage
    exit 1
fi

#echo "ACTION=$ACTION"
#echo "COMMAND=$COMMAND"
#echo "DESCR=$DESCR"
#echo "LABEL=$LABEL"
#echo "SNITCH_ID=$SNITCH_ID"
#exit;

if [ $ACTION == "test" ]; then
    testcommand
elif [ $ACTION == "run" ]; then
    runcommand
else
    usage
    exit 1 
fi

echo #
exit "$EXITVALUE"
